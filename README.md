# Expenses App Frontend

## How to run a dev server locally

1. Make sure you're using Node >= 18
1. Make sure you have [the backend](https://gitlab.com/expenses303508/back) running
1. Create a `.env` file following the format given in `.env.example` (here's is where you tell the app which backend to use)
1. Run `npm run dev`
