import { Severity } from "components/Toast/types";

export const NOTIFICATION_SEVERITY: { [key: string]: Severity } = {
  success: "success",
  error: "error",
};
