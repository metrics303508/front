import { Dispatch, SetStateAction } from "react";

import { Severity } from "components/Toast/types";

interface OpenNotificationParam {
  message: string;
  severity: Severity;
}

export type OpenNotification = (param: OpenNotificationParam) => void;

export type CloseNotification = () => void;

export interface NotificationsContextValue {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  severity: Severity;
  message: string;
  openNotification: OpenNotification;
  closeNotification: CloseNotification;
}
