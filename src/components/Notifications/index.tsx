import Toast from "components/Toast";

import { useNotifications } from "./hooks";

const Notifications = () => {
  const { open, setOpen, severity, message } = useNotifications();

  return (
    <Toast
      open={open}
      setOpen={setOpen}
      severity={severity}
      message={message}
    />
  );
};

export default Notifications;
