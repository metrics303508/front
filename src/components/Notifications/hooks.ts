import { useContext } from "react";

import { NotificationsContext } from "./context";
import { NotificationsContextValue } from "./types";

export const useNotifications = (): NotificationsContextValue => {
  return useContext(NotificationsContext) as NotificationsContextValue;
};
