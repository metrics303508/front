import {
  createContext,
  useState,
  ReactNode,
  SetStateAction,
  Dispatch,
} from "react";

import { Severity } from "components/Toast/types";

import { NOTIFICATION_SEVERITY } from "./constants";
import {
  OpenNotification,
  CloseNotification,
  NotificationsContextValue,
} from "./types";

const defaultNotificationsContextValue: NotificationsContextValue = {
  open: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setOpen: () => {},
  severity: "success",
  message: "",
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  openNotification: () => () => {},
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  closeNotification: () => () => {},
};

export const NotificationsContext = createContext<NotificationsContextValue>(
  defaultNotificationsContextValue
);

const openNotification =
  (
    setOpen: Dispatch<SetStateAction<boolean>>,
    setMessage: Dispatch<SetStateAction<string>>,
    setSeverity: Dispatch<SetStateAction<Severity>>
  ): OpenNotification =>
  ({ message, severity }) => {
    setMessage(message);
    setSeverity(severity);
    setOpen(true);
  };

const closeNotification =
  (setOpen: Dispatch<SetStateAction<boolean>>): CloseNotification =>
  () => {
    setOpen(false);
  };

interface NotificationsProviderProps {
  children: ReactNode;
}

const NotificationsProvider = ({ children }: NotificationsProviderProps) => {
  const [open, setOpen] = useState(false);
  const [severity, setSeverity] = useState<Severity>(
    NOTIFICATION_SEVERITY.success
  );
  const [message, setMessage] = useState("");

  return (
    <NotificationsContext.Provider
      value={{
        open,
        setOpen,
        severity,
        message,
        openNotification: openNotification(setOpen, setMessage, setSeverity),
        closeNotification: closeNotification(setOpen),
      }}
    >
      {children}
    </NotificationsContext.Provider>
  );
};

export default NotificationsProvider;
