import Typography from "@mui/material/Typography";

import classes from "./index.module.css";

const Footer = () => {
  return (
    <footer className={classes.footer}>
      <Typography>© {new Date().getFullYear()} Lual</Typography>
    </footer>
  );
};

export default Footer;
