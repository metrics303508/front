import { useQuery } from "@tanstack/react-query";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";

import api from "api";
import { UNIQUE_KEYS } from "queryClient/constants";
import { APIExpense } from "types";

import classes from "./index.module.css";

interface ExpensesTableProps {
  data: APIExpense[];
}

const ExpensesTable = ({ data }: ExpensesTableProps) => {
  return (
    <TableContainer className={classes.expensesTable}>
      <Table sx={{ minWidth: 650 }} aria-label="Expenses table">
        <TableHead>
          <TableRow>
            <TableCell align="left">Timestamp</TableCell>
            <TableCell align="left">Name</TableCell>
            <TableCell align="left">Value</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(({ timestamp, name, value }: APIExpense, i: number) => (
            <TableRow key={i}>
              <TableCell align="left">{timestamp}</TableCell>
              <TableCell align="left">{name}</TableCell>
              <TableCell align="left">{value}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
const ExpensesList = () => {
  const { data } = useQuery([UNIQUE_KEYS.expenses], api.expenses.getAll);

  return <>{(data ?? []).length > 0 && <ExpensesTable data={data as APIExpense[]} />}</>;
};

export default ExpensesList;
