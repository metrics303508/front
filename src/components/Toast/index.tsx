import { Dispatch, SetStateAction } from "react";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import Slide, { SlideProps } from "@mui/material/Slide";

import { Severity } from "./types";

const handleClose = (setOpen: Dispatch<SetStateAction<boolean>>) => () => {
  setOpen(false);
};

const SnackbarSlide = (props: SlideProps) => {
  return <Slide {...props} direction="left" />;
};

interface ToastProps {
  open: boolean;
  severity: Severity;
  message: string;
  setOpen: Dispatch<SetStateAction<boolean>>;
}

const Toast = ({ open, severity="success", setOpen, message }: ToastProps) => {
  return (
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handleClose(setOpen)}
        TransitionComponent={SnackbarSlide}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <Alert
          elevation={4}
          severity={severity}
          action={
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleClose(setOpen)}
            >
              <CloseIcon />
            </IconButton>
          }
        >
          {message}
        </Alert>
      </Snackbar>
  )
};

export default Toast;

