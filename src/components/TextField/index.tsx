import {
  Context,
  useState,
  ChangeEvent,
  Dispatch,
  SetStateAction,
} from "react";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";

import { useField } from "lib/forms/hooks";
import { FormStateSetter, FormContext, Validator } from "lib/forms/types";
import { buildFormUpdateFor } from "lib/forms/utils";

const handleFocus = (handleFocus: Dispatch<SetStateAction<boolean>>) => () => {
  handleFocus(true);
};

const handleBlur =
  (
    handleFocus: Dispatch<SetStateAction<boolean>>,
    setFormState: FormStateSetter,
    valid: Validator,
    value: string,
    name: string
  ) =>
  () => {
    if (valid(value)) {
      setFormState(buildFormUpdateFor(name, { value, error: false }));
    } else {
      setFormState(buildFormUpdateFor(name, { value, error: true }));
    }

    handleFocus(false);
  };

const handleChange =
  (setFormState: FormStateSetter, name: string) =>
  (e: ChangeEvent<HTMLInputElement>) => {
    setFormState(buildFormUpdateFor(name, { value: e.target.value }));
  };

interface TextFieldProps {
  label: string;
  name: string;
  formContext: Context<FormContext>;
  validator: Validator;
}

const TextField = ({ label, name, formContext, validator }: TextFieldProps) => {
  const [focused, setFocused] = useState(false);
  const { value, setFormState } = useField(name, formContext);

  return (
    <>
      <InputLabel focused={focused}>{label}</InputLabel>
      <OutlinedInput
        fullWidth
        onChange={handleChange(setFormState, name)}
        onFocus={handleFocus(setFocused)}
        onBlur={handleBlur(setFocused, setFormState, validator, value, name)}
        value={value}
      />
    </>
  );
};

export default TextField;
