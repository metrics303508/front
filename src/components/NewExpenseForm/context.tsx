import { createContext, ReactNode } from "react";

import { FormContext } from "lib/forms/types";

import { INITIAL_FORM_STATE } from "./constants";

export const NewExpenseFormContext = createContext<FormContext>([
  INITIAL_FORM_STATE,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  () => {},
]);

interface NewExpenseFormContextProviderProps {
  children: ReactNode;
  value: FormContext;
}

export const NewExpenseFormContextProvider = ({
  children,
  value,
}: NewExpenseFormContextProviderProps) => {
  return (
    <NewExpenseFormContext.Provider value={value}>
      {children}
    </NewExpenseFormContext.Provider>
  );
};
