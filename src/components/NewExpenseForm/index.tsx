import {
  useMutation,
  useQueryClient,
  UseMutationResult,
} from "@tanstack/react-query";
import { useState, FormEvent } from "react";
import { grey } from "@mui/material/colors";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";

import { APIExpense } from "types";
import { notEmpty, isNumberString, noErrorsIn } from "lib/forms/validators";
import { formStateToFormData } from "lib/forms/utils";
import { FormData, FormState, FormStateSetter } from "lib/forms/types";
import api from "api";
import { UNIQUE_KEYS } from "queryClient/constants";
import TextField from "components/TextField";
import { useNotifications } from "components/Notifications/hooks";
import { OpenNotification } from "components/Notifications/types";

import { FIELD_NAMES, INITIAL_FORM_STATE } from "./constants";
import { NewExpenseFormContextProvider, NewExpenseFormContext } from "./context";
import classes from "./index.module.css";

const handleSubmit =
  (
    formState: FormState,
    setFormState: FormStateSetter,
    mutation: UseMutationResult<APIExpense, unknown, FormData>,
    openNotification: OpenNotification
  ) =>
  (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (noErrorsIn(formState)) {
      mutation.mutate({
        ...formStateToFormData(formState),
        timestamp: new Date().toISOString(),
      });
    } else {
      openNotification({
        severity: "error",
        message: "Some fields are invalid",
      });
    }
  };

const NewExpenseForm = () => {
  const [formState, setFormState] = useState<FormState>(INITIAL_FORM_STATE);
  const { openNotification } = useNotifications();
  const queryClient = useQueryClient();

  const postMutation = useMutation<APIExpense, unknown, FormData>({
    mutationFn: api.expenses.post,
    onSuccess: (data) => {
      openNotification({ severity: "success", message: "Expense saved!" });
      setFormState(INITIAL_FORM_STATE);
      queryClient.setQueryData(
        [UNIQUE_KEYS.expenses],
        (prev: APIExpense[] = []) => {
          return [{ ...data }, ...prev];
        }
      );
    },
    onError: () =>
      openNotification({
        severity: "error",
        message: "An error occurred while saving the expense",
      }),
  });

  return (
    <NewExpenseFormContextProvider value={[formState, setFormState]}>
      <Card
        elevation={10}
        className={classes.newExpenseForm}
        sx={{ border: `1px solid ${grey[200]}` }}
      >
        <Grid
          component="form"
          onSubmit={handleSubmit(
            formState,
            setFormState,
            postMutation,
            openNotification
          )}
          container
          spacing={2}
          alignItems="flex-end"
        >
          <Grid item xs={12} sm={5} md={5}>
            <TextField
              label="Name"
              name={FIELD_NAMES.name}
              formContext={NewExpenseFormContext}
              validator={notEmpty}
            />
          </Grid>
          <Grid item xs={12} sm={5} md={5}>
            <TextField
              label="Value"
              name={FIELD_NAMES.value}
              formContext={NewExpenseFormContext}
              validator={isNumberString}
            />
          </Grid>
          <Grid item xs={12} sm={2} md={2}>
            <Button variant="contained" type="submit" fullWidth>
              Save
            </Button>
          </Grid>
        </Grid>
      </Card>
    </NewExpenseFormContextProvider>
  );
};

export default NewExpenseForm;
