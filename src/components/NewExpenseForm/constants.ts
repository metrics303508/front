export const FIELD_NAMES = {
  name: "name",
  value: "value",
};

export const INITIAL_FORM_STATE = {
  [FIELD_NAMES.name]: {
    value: "",
    error: true,
  },
  [FIELD_NAMES.value]: {
    value: "",
    error: true,
  },
};
