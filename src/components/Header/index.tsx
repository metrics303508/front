import Typography from "@mui/material/Typography";

import classes from "./index.module.css";

const Header = () => {
  return (
    <header className={classes.header}>
      <Typography variant="h1">Expenses</Typography>
    </header>
  );
};

export default Header;
