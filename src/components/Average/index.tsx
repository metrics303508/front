import { grey } from "@mui/material/colors";
import clsx from "clsx";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";

import classes from "./index.module.css";

interface AverageProps {
  label: string;
  value: string;
}

const Average = ({ label, value }: AverageProps) => {
  return (
    <Card elevation={0} sx={{ background: grey[100] }}>
      <Typography className={clsx(classes.value, classes.override)}>
        {value}
      </Typography>
      <Typography
        variant="h3"
        className={clsx(classes.label, classes.override)}
        sx={{ color: grey[700] }}
      >
        Average per {label}
      </Typography>
    </Card>
  );
};

export default Average;
