import moment from "moment";

import { APIExpense } from "types";
import { AveragesData } from "./types";

// Used because diff acts as a minus
const DAY_CORRECTION = 1;

export const calculateAverages = (
  expenses: APIExpense[] = []
): AveragesData => {
  if (expenses.length === 0) {
    return {
      day: "--",
      week: "--",
      month: "--",
    };
  }

  const newest = moment((expenses.at(-1) as APIExpense).timestamp);
  const oldest = moment((expenses.at(0) as APIExpense).timestamp);

  const totalSumOfValues = expenses.reduce((acc, { value }) => acc + value, 0);

  const totalDays = Math.abs(newest.diff(oldest, "days")) + DAY_CORRECTION;
  const totalWeeks = Math.abs(newest.diff(oldest, "weeks"));
  const totalMonths = Math.abs(newest.diff(oldest, "months"));

  return {
    day: (totalSumOfValues / totalDays).toFixed(2),
    week: (totalSumOfValues / (totalWeeks > 0 ? totalWeeks : 1)).toFixed(2),
    month: (totalSumOfValues / (totalMonths > 0 ? totalMonths : 1)).toFixed(2),
  };
};
