import { useMemo } from "react";
import { useQuery } from "@tanstack/react-query";
import Average from "components/Average";
import Grid from "@mui/material/Grid";

import api from "api";
import { UNIQUE_KEYS } from "queryClient/constants";

import { AVG_LABEL } from "./constants";
import { calculateAverages } from "./utils";
import classes from "./index.module.css";

const Averages = () => {
  const { data: expenses } = useQuery([UNIQUE_KEYS.expenses], api.expenses.getAll);

  const {
    day: dayAvg,
    week: weekAvg,
    month: monthAvg,
  } = useMemo(() => calculateAverages(expenses), [expenses]);

  return (
    <Grid
      component="section"
      className={classes.averages}
      container
      spacing={2}
    >
      <Grid item xs={12} sm={6} md={4}>
        <Average label={AVG_LABEL.day} value={dayAvg} />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <Average label={AVG_LABEL.week} value={weekAvg} />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <Average label={AVG_LABEL.month} value={monthAvg} />
      </Grid>
    </Grid>
  );
};

export default Averages;
