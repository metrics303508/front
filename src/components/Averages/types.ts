export interface AveragesData {
  day: string;
  week: string;
  month: string;
}
