import { describe, it, expect } from "vitest";

import { APIExpense } from "types";

import { calculateAverages } from "./utils";

const expenses: APIExpense[] = [
  {
    name: "name",
    value: 150.5,
    timestamp: "2023-03-01T22:00:00.000Z",
    _v: 0,
    _id: "00000000",
  },
  {
    name: "name",
    value: 49.5,
    timestamp: "2023-03-02T01:00:00.000Z",
    _v: 0,
    _id: "00000001",
  },
  {
    name: "name",
    value: 100,
    timestamp: "2023-03-31T06:00:00.000Z",
    _v: 0,
    _id: "00000002",
  },
];

describe("calculateAverages", () => {
  it("returns expected average per day", () => {
    expect(calculateAverages(expenses).day).toBe("10.00");
  });

  it("returns expected average per week", () => {
    expect(calculateAverages(expenses).week).toBe("75.00");
  });

  it("returns expected average per month", () => {
    expect(calculateAverages(expenses).month).toBe("300.00");
  });
});
