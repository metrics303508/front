import ExpensesList from "components/ExpensesList";
import NewExpenseForm from "components/NewExpenseForm";

const Expenses = () => {
  return (
    <section>
      <NewExpenseForm />
      <ExpensesList />
    </section>
  )
};

export default Expenses;
