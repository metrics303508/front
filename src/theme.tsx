import { ReactNode } from "react";
import { grey, teal } from "@mui/material/colors";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const palette = {
  primary: {
    main: teal[900],
  },
  text: {
    primary: "rgba(0, 0, 0, 0.87)",
    secondary: grey[700],
  },
};

const theme = createTheme({
  typography: {
    fontFamily: "IBM Plex Sans",
    h1: {
      fontSize: 40,
      fontWeight: 700,
      color: palette.primary.main,
    },
    body1: {
      fontSize: 20,
    },
  },
  palette: {
    ...palette,
  },
  shadows: [
    "none",
    "",
    "",
    "",
    "0px 2px 4px -1px rgba(0,0,0,0.1),0px 4px 5px 0px rgba(0,0,0,0.07),0px 1px 10px 0px rgba(0,0,0,0.06)",
    "",
    "",
    "",
    "",
    "",
    "0px 0px 16px 8px rgba(0,0,0,0.07)",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  ],
  components: {
    MuiAlert: {
      styleOverrides: {
        action: {
          position: "relative",
          top: 1,
        },
        icon: {
          position: "relative",
          top: 4,
        },
        message: {
          fontSize: 20,
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: 20,
          fontWeight: 700,
          borderRadius: 8,
          lineHeight: 1,
          padding: "12px 16px",
        },
      },
    },
    MuiCard: {
      styleOverrides: {
        root: {
          padding: 16,
          borderRadius: 16,
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          margin: "0 0 4px 4px",
          color: palette.text.secondary,
          fontWeight: 600,
          "&.Mui-focused": {
            fontWeight: 600,
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        input: {
          padding: 8,
        },
        root: {
          borderRadius: 8,
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          fontSize: 20,
          borderBottom: `1px solid ${grey[400]}`,
        },
        head: {
          fontWeight: 600,
          background: grey[100],
        },
      },
    },
    MuiTableContainer: {
      styleOverrides: {
        root: {
          border: `1px solid ${grey[400]}`,
        },
      },
    },
    MuiTableRow: {
      styleOverrides: {
        root: {
          "&:last-child > .MuiTableCell-body": {
            borderBottom: 0,
          },
        },
      },
    },
  },
});

interface ThemeProps {
  children: ReactNode;
}

const Theme = ({ children }: ThemeProps) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default Theme;
