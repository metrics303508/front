import { FormData, FieldState, FormState } from "./types";

export const buildFormUpdateFor =
  (fieldName: string, newFieldState: Partial<FieldState>) =>
  (prev: FormState): FormState => {
    return {
      ...prev,
      [fieldName]: {
        ...prev[fieldName],
        ...newFieldState,
      },
    };
  };

export const formStateToFormData = (formState: FormState): FormData => {
  return Object.fromEntries(
    Object.entries(formState).map(([fieldName, { value }]) => [
      fieldName,
      value,
    ])
  );
};
