import { FormState, FieldState } from "./types";

export const notEmpty = (value: string): boolean => {
  return typeof value === "string" && value !== "";
};

const NUMBER_REGEX = /^-?\d+(\.\d+)?$/;

export const isNumberString = (value: string): boolean => {
  return typeof value === "string" && NUMBER_REGEX.test(value);
};

export const noErrorsIn = (formState: FormState): boolean => {
  return !Object.values(formState).some(
    (fieldState: FieldState) => fieldState.error
  );
};
