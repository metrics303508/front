import { Dispatch, SetStateAction } from "react";

export interface FieldState {
  value: string;
  error: boolean;
}

export interface FormState {
  [fieldName: string]: FieldState;
}

export type FormStateSetter = Dispatch<SetStateAction<FormState>>;

export interface FieldContext extends FieldState {
  setFormState: FormStateSetter;
}

export type FormContext = [FormState, FormStateSetter];

export interface FormData {
  [fieldName: string]: string;
}

export type Validator = (value: string) => boolean;
