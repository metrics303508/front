import { Context, useContext } from "react";

import { FormContext, FieldContext } from "./types";

export const useField = (
  fieldName: string,
  formContext: Context<FormContext>
): FieldContext => {
  const [formState, setFormState] = useContext<FormContext>(formContext);

  return {
    error: formState[fieldName].error,
    value: formState[fieldName].value,
    setFormState,
  };
};
