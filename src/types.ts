export type IsoDateString =
  `${number}-${number}-${number}T${number}:${number}:${number}.${number}Z`;

export interface Expense {
  timestamp: IsoDateString;
  name: string;
  value: number;
}

export interface APIExpense {
  timestamp: IsoDateString;
  name: string;
  value: number;
  _id: string;
  _v: number;
}
