import Averages from "components/Averages";
import Footer from "components/Footer";
import Header from "components/Header";
import Expenses from "components/Expenses";
import Notifications from "components/Notifications";
import NotificationsProvider from "components/Notifications/context";

import ThemeProvider from "./theme";
import QueryClientProvider from "./queryClient";

import classes from "./index.module.css";

function App() {
  return (
    <ThemeProvider>
      <NotificationsProvider>
        <QueryClientProvider>
          <Notifications />
          <div className={classes.wrapper}>
            <Header />
            <Averages />
            <Expenses />
            <Footer />
          </div>
        </QueryClientProvider>
      </NotificationsProvider>
    </ThemeProvider>
  );
}

export default App;
