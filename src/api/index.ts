import axios from "axios";

import { APIExpense } from "types";
import { FormData } from "lib/forms/types";

import { ENDPOINTS } from "./constants";

const api = {
  expenses: {
    post: (data: FormData): Promise<APIExpense> => {
      return axios.post(ENDPOINTS.expenses, data).then(({ data }) => data);
    },
    getAll: (): Promise<APIExpense[]> => {
      return axios.get(ENDPOINTS.expenses).then(({ data }) => data);
    },
  },
};

export default api;
