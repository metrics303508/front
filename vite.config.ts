import path from "path";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: [
      {
        find: "api",
        replacement: path.resolve(__dirname, "src/api"),
      },
      {
        find: "components",
        replacement: path.resolve(__dirname, "src/components"),
      },
      {
        find: "lib",
        replacement: path.resolve(__dirname, "src/lib"),
      },
      {
        find: "queryClient",
        replacement: path.resolve(__dirname, "src/queryClient"),
      },
      {
        find: "types",
        replacement: path.resolve(__dirname, "src/types.ts"),
      },
    ],
  },
});
